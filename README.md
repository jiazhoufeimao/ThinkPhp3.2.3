#ThinkPhp3.2.3

本框架是我尝试Php+Java进行结合的尝试.PHP在Web开发中 方便快捷高效,Java和Php各有各的优势.让我们尝试玩一下.

--------

#ThinkPHP目录

[+]ThinkPHP
    [+]adminse ------- 后台管理

    [+]app     ------- 前台框架

    [+]ThinkPHP------- ThinkPHP核心框架

    [-]index.php ----- 前台首页

    [-]admin.php ----- 后台首页

目录说明：
    Common  公共函数库

    Home    Model、Controller等

    Runtime 运行时环境(日志、缓存等文件)